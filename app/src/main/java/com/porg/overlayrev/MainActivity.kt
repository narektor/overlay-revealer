package com.porg.overlayrev

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val indicator = resources.getBoolean(R.bool.indicator)
        val string = resources.getString(R.string.overlayString)
        if (indicator)
            Toast.makeText(applicationContext, "indicator = true, overlaid!", Toast.LENGTH_LONG).show()
        else
            Toast.makeText(applicationContext, "indicator = false, not overlaid!", Toast.LENGTH_LONG).show()
    }
}