# Overlay Revealer

A small app that detects resource overlays. Can be used to check if an overlay manager works.

Originally made to test if fabricated RROs were blocked in the first (and only) developer preview of Android 12L.

This is an older, less complete app that won't be updated frequently. I originally didn't want to release it, or release a more complete version as proprietary, but decided to release the source code under the GPL.

To test if an overlay was applied, overlay `bool/indicator`, apply the overlay and open the app. There should be a toast saying if the boolean has been overlaid.
